# GitLab VSCode Fork

This project produces builds of the [VSCode](https://github.com/microsoft/vscode) editor. GitLab uses 
these builds to power the [GitLab's Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) and 
[GitLab Workspaces](https://docs.gitlab.com/ee/user/workspace/) products.

We customize VSCode by applying patches stored in the [`vscode-patches`](./vscode-patches/)
directory. These patches tailor the editing experience toward the use cases enabled by the GitLab Web IDE 
and GitLab Workspaces.

## Getting started

- [Contributing guidelines](./CONTRIBUTING.md)
- [GitLab VSCode Fork releases](./docs/releases.md)
- [Updating base VSCode version](./docs/updating-vscode.md)
- [Changelog](./CHANGELOG.md)

## Related projects

- [gitlab-web-ide](https://gitlab.com/gitlab-org/gitlab-web-ide/)
- [gitlab-workspaces-tools](https://gitlab.com/gitlab-org/workspaces/gitlab-workspaces-tools)

## Legal

This project is not affiliated with Microsoft Corporation.
