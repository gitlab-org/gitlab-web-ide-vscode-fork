## Development Guide

For updating the VSCode version, refer to [this doc](./updating-vscode.md) instead.

### Setup

- In the project root, run ` git submodule update --init --depth 1` (If the vscode submodule is empty).
- In the project root, run `./scripts/dev-apply-patches.sh web-ide` (or `./scripts/dev-apply-patches.sh workspaces` for workspaces version).
- `cd` into `vscode` submodule and confirm that patches are applied by running `git status`.
- If you use VSCode to work on this project, set the code formatter to `Typescript & Javascript Language Feature`: command + shift + P, type `Format document with` and select `Typescript & Javascript Language Feature`.

### Making changes

Follow these rules to clearly identify our customizations of the VSCode editor. This is useful to include as it provides more context on whether the change was introduced by the upstream project or GitLab.

- Any new files **must** be prefixed with `gl_` or `GL_`, with the exception of files that require a specific name (e.g. `README.md` or `.gitlab-ci.yml`).
- Changes made to pre-existing files must be surrounded in commented regions, starting with `gl_mod START` and ending with `gl_mod END`. Please include a reason for the change with links for more context. For example:

  ```patch
   function vscodeFoo(a, b) {
     const c = a.ref || 'DEFAULT';

     return {
       c,
  -    disabled: false,
  +    // gl_mod START
  +    // We need this `disabled` flag to be true, otherwise
  +    // users are able to accidentally break everything.
  +    // See issue: https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/12
  +    disabled: true
  +    // gl_mod END
     };
   }
  ```

This is useful to include as it provides more context on whether the change was introduced by the upstream project or GitLab.

### Testing changes locally in GitLab Web IDE

- In the project root, run `./scripts/build-and-bundle-web.sh`
- After the script above successfully completes, follow the steps in the `gitlab-web-ide` repository to integrate with local VSCode repo.

Building VSCode is a time consuming process. To speed up further builds, follow these instructions:

1. In a terminal, start the watch task `yarn run watch`
2. In another terminal, start the web watch task `yarn run watch-web`
3. When a change has been made, run `yarn run gulp compile-build-pr`

### Committing changes

- Within the `vscode` submodule, commit your changes with conventional commit message.
- Generate a patch of the change by running `../scripts/dev-save-patches.sh <CONTEXT>` where `<CONTEXT>` is either `web-ide` or `workspaces` (depending what you used to apply the patches).
- Discard the `vscode` submodule changes by running `git submodule update` before staging and committing the new patch files.
