# GitLab VSCode Fork releases

## Version format

Here's an example of a version for the artifacts deployed from this project:

```
1.69.1-0.0.1-dev-20220722150450
``` 

- `1.69.1` is the base VSCode version
- `0.0.1` is the version of this fork
- `dev` is a flag that the release is meant only for development purposes.
  This could be replaced with `rc` if the artifact was a release candidate.
- `20220722150450` is the timestamp of the artifacts creation

We'll use GitLab releases to host these release artifacts and create tags based
on the version name of the artifact created in the `main` branch.

**Please note:** Official releases will not have `rc`, `dev`, or timestamp extension.

## Creating release

Only maintainers can create a release (developers can't push tags).

1. Create tag locally using the `./scripts/create-release-tag.sh <dev|prod>`.

   - Use `dev` argument if you are making dev release.
   - Use `prod` argument if you are making production release.

1. Push the tag (e.g. `git push origin 1.71.1-1.0.0-dev-20221003100000`).
1. When the pipeline succeeds:
   1. Find the release for your version on the [releases page](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/releases).
   1. Find the package version on the [package registry page](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/packages).
   1. Edit the release description and add the link to the released package version.
