# Updating base VSCode version

The GitLab VSCode Fork project produces [VSCode](https://github.com/microsoft/vscode) builds by
running VSCode build commands from a Git submodule. The VSCode Git submodule lives in the `vscode`
directory, and it's pinned to a release tag, e.g. `1.89.0`.

This document describes the process to update the pinned VSCode version and how to test it on
dependant projects. The process is organized in three phases:

1. Update VSCode submodule.
1. Exploratory testing.
1. Release updated version.

## Update VSCode submodule

The following instructions update VSCode to version 1.92.0. You can adapt the instructions to any VSCode version.

1. If it is the your first time cloning the GitLab VSCode repository,
initialize the `vscode` submodule.

   ```sh
   git submodule update --init --depth 1
   ```

1. Create an upgrade branch where you will commit the update changes.

   ```sh
   git checkout -b update-vscode-to-1.92.0
   ```

1. Access the vscode submodule and fetch all remote objects.

   ```sh
   cd vscode
   git fetch
   ```

1. Checkout the tag that targets the new VSCode version. In the context of this
example is 1.92.0.

   ```sh
   git checkout 1.92.0
   ```

1. Update the VERSION file and commit the new submodule pinned tag.

   ```sh
   cd ..
   echo "1.92.0-1.0.0" > VERSION
   git commit -m "Update VSCode to 1.92.0"
   ```

### Applying GitLab's customizations

Apply the patches containing GitLab's customizations. You might run into
merge conflicts when applying the patches. If conflicts occur, and you don't
know how to resolve them due to a lack of context,
reach out to a GitLab VSCode Fork maintainer for assistance.

There are two distinct sets of patches: one set applies exclusively to the VSCode 
build used by _GitLab Web IDE_, while the other set applies exclusively to the 
build used by _GitLab Workspaces_. **You should run this process for both builds on every update**.

1. Apply the patches for a specific project build.

   ```sh
   ./scripts/dev-apply-patches.sh [web-ide|workspaces]
   ```

   **NOTE**: You have to enter the subbmodule directory to resolve the git
conflicts (`cd vscode && git mergetool`)


1. After applying patches and completing merge conflict resolution,
we have to generate new patch files and replace existing ones to persist
the changes generated by the conflict resolution.

   ```sh
   ./scripts/dev-save-patches.sh [web-ide|workspaces]
   ```

1. After saving the patches for a build type (i.e. `web-ide`), reset the submodule state
and start the process again for the other build type.

   ```sh
   git submodule update -f
   ```

1. After updating all the patch files, move to the
root repository and add new patch files to the staging area:

   ```sh
   git add vscode-patches
   ```

   **NOTE** _Don't_ add the updated vscode submodule reference to the staging
area. When running `git status`, it looks like this:

   ```sh
   modified:   vscode (new commits)
   ```

1. Commit the updated patch files and push the changes to the
remote repository.

   ```sh
   git commit --amend
   git push origin update-vscode-to-1.92.0
   ```

1. Create a merge request for the update branch `update-vscode-to-1.92.0`. Wait until
the MR's pipeline succeeds and follow the instructions in the exploratory testing
phase to ensure that the new VSCode version doesn't introduce regressions
on the Web IDE or Workspaces.

## Exploratory testing

Pending: We will write the exploratory testing documentation in
https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/46

## Release updated version

Follow the [releases documentation](./releases.md) to publish a new release
with the updated VSCode.
