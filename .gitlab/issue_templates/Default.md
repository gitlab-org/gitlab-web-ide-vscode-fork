Please use one of the following issue description templates:

- **Web IDE Category** - if the issue pertains mostly to the Web IDE feature.
- **Workspaces Category** - if the issue pertains mostly to the Workspaces feature.

Otherwise, please leave as much information as possible in this issue. Thank you for helping to make GitLab a better product.
