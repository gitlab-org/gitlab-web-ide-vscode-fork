From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Paul Slaughter <pslaughter@gitlab.com>
Date: Wed, 8 Feb 2023 11:52:51 -0600
Subject: [PATCH 3/8] fix: Always display extensions marketplace welcome view

- with the purpose of displaying a message indicating that
  it is temporarily disabled in the GitLab Web IDE.
- https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/merge_requests/26
---
 .../contrib/extensions/browser/extensionsViewlet.ts | 13 +++++++++++--
 .../contrib/extensions/browser/extensionsViews.ts   | 10 ++++++++++
 2 files changed, 21 insertions(+), 2 deletions(-)

diff --git a/src/vs/workbench/contrib/extensions/browser/extensionsViewlet.ts b/src/vs/workbench/contrib/extensions/browser/extensionsViewlet.ts
index 9a81262ecf8f..9ac823c1e4d0 100644
--- a/src/vs/workbench/contrib/extensions/browser/extensionsViewlet.ts
+++ b/src/vs/workbench/contrib/extensions/browser/extensionsViewlet.ts
@@ -65,6 +65,10 @@ import { registerNavigableContainer } from 'vs/workbench/browser/actions/widgetN
 import { MenuWorkbenchToolBar } from 'vs/platform/actions/browser/toolbar';
 import { createActionViewItem } from 'vs/platform/actions/browser/menuEntryActionViewItem';
 
+// gl_mod START add `productService` so we can add condition based on if extensionsGallery is avialable or not
+import { IProductService } from 'vs/platform/product/common/productService';
+// gl_mod END
+
 export const DefaultViewsContext = new RawContextKey<boolean>('defaultExtensionViews', true);
 export const ExtensionsSortByContext = new RawContextKey<string>('extensionsSortByValue', '');
 export const SearchMarketplaceExtensionsContext = new RawContextKey<boolean>('searchMarketplaceExtensions', false);
@@ -94,7 +98,10 @@ export class ExtensionsViewletViewsContribution extends Disposable implements IW
 		@IExtensionManagementServerService private readonly extensionManagementServerService: IExtensionManagementServerService,
 		@ILabelService private readonly labelService: ILabelService,
 		@IViewDescriptorService viewDescriptorService: IViewDescriptorService,
-		@IContextKeyService private readonly contextKeyService: IContextKeyService
+		@IContextKeyService private readonly contextKeyService: IContextKeyService,
+		// gl_mod START add `productService` so we can add condition based on if extensionsGallery is avialable or not
+		@IProductService private readonly productService: IProductService,
+		// gl_mod END
 	) {
 		super();
 
@@ -168,7 +175,9 @@ export class ExtensionsViewletViewsContribution extends Disposable implements IW
 				weight: 100,
 				order: 1,
 				when: ContextKeyExpr.and(DefaultViewsContext),
-				ctorDescriptor: new SyncDescriptor(ServerInstalledExtensionsView, [{ server, flexibleHeight: true, onDidChangeTitle }]),
+				// gl_mod START Use `productService` to determine `flexibleHeight`
+				ctorDescriptor: new SyncDescriptor(ServerInstalledExtensionsView, [{ server, flexibleHeight: Boolean(this.productService.extensionsGallery), onDidChangeTitle }]),
+				// gl_mod END
 				/* Installed extensions views shall not be allowed to hidden when there are more than one server */
 				canToggleVisibility: servers.length === 1
 			});
diff --git a/src/vs/workbench/contrib/extensions/browser/extensionsViews.ts b/src/vs/workbench/contrib/extensions/browser/extensionsViews.ts
index f2eea7c616d4..4456404d0173 100644
--- a/src/vs/workbench/contrib/extensions/browser/extensionsViews.ts
+++ b/src/vs/workbench/contrib/extensions/browser/extensionsViews.ts
@@ -1291,6 +1291,16 @@ export class DefaultPopularExtensionsView extends ExtensionsListView {
 }
 
 export class ServerInstalledExtensionsView extends ExtensionsListView {
+	// gl_mod START
+	// This is a temporary change to display a message in the
+	// Extensions Marketplace welcome view indicating that it is
+	// temporarily disabled.
+	override shouldShowWelcome(): boolean {
+		const hasExtensionsGallery = Boolean(this.productService.extensionsGallery);
+
+		return !hasExtensionsGallery;
+	}
+	// gl_mod END
 
 	override async show(query: string): Promise<IPagedModel<IExtension>> {
 		query = query ? query : '@installed';
