#!/usr/bin/env bash

# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.

# Import common variables
SCRIPTS_DIR=$(dirname $(readlink -f $0))
source "$SCRIPTS_DIR/base.sh"

source "$SCRIPTS_DIR/patch-product-json.sh"

output_dir="${build_dir}/vscode-reh-web"

cd $vscode_dir

echo "----------------"
echo "Build GitLab VSCode Fork server"
echo "----------------"

yarn monaco-compile-check
NODE_OPTIONS='--max-old-space-size=4095' yarn valid-layers-check

yarn gulp core-ci
# The next 3 tasks can replace the `gulp core-ci` once we don't need the
# VS Code Web UI packaged in the server https://gitlab.com/gitlab-org/gitlab/-/issues/393038
# yarn gulp compile-build
# yarn gulp minify-vscode
# yarn gulp minify-vscode-reh

# The extension tasks are necessary
# Without them the server doesn't start because it's missing extensions
yarn gulp compile-extension-media
yarn gulp compile-extensions-build

declare -a os_and_archs=(
	"darwin-x64"
	"darwin-arm64"
	"win32-x64"
	"linux-x64"
	"linux-arm64"
)

for os_and_arch in "${os_and_archs[@]}"
do
	yarn gulp "vscode-reh-web-${os_and_arch}-min-ci"

	BUNDLE_NAME=vscode-reh-web-${os_and_arch}-${full_version}
	mkdir -p $output_dir

	bundle_and_build "${root_dir}/vscode-reh-web-${os_and_arch}" "${output_dir}/${BUNDLE_NAME}.tar.gz"
done
