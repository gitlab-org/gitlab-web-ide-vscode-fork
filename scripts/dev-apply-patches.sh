set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.

# Import common variables
SCRIPTS_DIR=$(dirname $(readlink -f $0))
source "$SCRIPTS_DIR/base.sh"

patches_context="${1:-}"
patches_context=$(get_patch_context "${patches_context}")

echo "[gitlab-web-ide-vscode-fork] Starting git am to apply patches..."
echo ""
echo " If you run into conflicts, open the vscode folder in a separate terminal, use regular git tools to resolve and run 'git am --continue'."
echo ""

apply_patches $patches_context
