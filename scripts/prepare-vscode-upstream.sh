#!/usr/bin/env bash

# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.

# Import common variables
SCRIPTS_DIR=$(dirname $(readlink -f $0))
source "$SCRIPTS_DIR/base.sh"

patches_context="${1:-}"
patches_context=$(get_patch_context "${patches_context}")

echo "--------------"
echo "Applying patches to upstream project"
echo "---------------"

function cleanup_submodule() {
	cd $root_dir
	git submodule deinit -f vscode
	git submodule update --init -f --depth 1
	cd $vscode_dir
	git clean -df
	cd $root_dir
}

if [ -n "${CI_JOB_ID+x}" ]; then
	# Reset upstream submodule to its initial state to make this script idempotent
	cleanup_submodule
else
	read -p "Would you like to reset the submodule state. All changes applied to the submodule WILL BE LOST (y/N)? " reset

	if [[ "$reset" == "y" ]]; then
		cleanup_submodule
	fi
fi

apply_patches $patches_context

if [ $? -ne 0 ]; then
	echo "Could not patch upstream project. Verify if there are merge conflicts that needs to be solved."
else
	echo "Upstream submodule was patched successfully"
fi

# Install dependencies
echo "--------------"
echo "Installing yarn dependencies"
echo "---------------"
yarn --frozen-lockfile --check-files

cd $root_dir
