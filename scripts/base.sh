#!/usr/bin/env bash

# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.

function bundle_and_build(){
	local build_directory="$1"
	local build_tarball="$2"

	# note: Add commit to build artifact since this needs to be used when consuming the artifact
	#       This is related to `gitlab-static.net` CDN hosting for VSCode sandboxing
	git rev-parse HEAD > "${build_directory}/COMMIT.txt"

	tar -czvf "${build_tarball}" -C "${build_directory}" .
}

function get_patch_context(){
	local user_input="$1"

	if [[ "$user_input" != "web-ide" && "$user_input" != "workspaces" ]]; then
		printf "This script needs context for either 'web-ide' or 'workspaces'. Please enter either 'web-ide' or 'workspaces': " >&2
		read -r user_input
	fi

	while [[ "$user_input" != "web-ide" && "$user_input" != "workspaces" ]]; do
		printf "Invalid input. Please enter either 'web-ide' or 'workspaces': " >&2
		read -r user_input
	done

	printf "%s" "$user_input"
}

function get_vscode_target_sha(){
	local result=$(cd $root_dir && git ls-tree --format='%(objectname)' HEAD -- vscode)

	printf "%s" "$result"
}

function get_common_delimiter_sha(){
	local result=$(cd $vscode_dir && git log --all --grep='--- common patches end ---' --format="%H" -n 1)

	printf "%s" "$result"
}

function invoke_git_am() {
	# Adds --no-verify in local environment to prevent commit hooks from running
	if [[ -z "${CI_API_V4_URL:-}" ]]; then
		git am -3 --no-verify $1
	else
		git am -3 $1
	fi
}

function apply_patches(){
	local patches_context="$1"
	local common_patches_path="$patches_dir/common/*"
	local patches_context_path="$patches_dir/$patches_context/*"

	cd $vscode_dir

	local common_delimiter_applied=$(git log -1 --grep "$common_delimiter_commit_message")
	local last_commit_message=$(git log -1 --pretty=%B | head -n 1)
	local common_commits_applied=$(grep "$last_commit_message" "$common_patches_path")

	if [[ -n "$common_commits_applied" || -n "$common_delimiter_applied" ]]; then
		local reapply_or_continue=""

		while [[ "$reapply_or_continue" != "1" && "$reapply_or_continue" != "2" ]]; do
			printf "It looks like the common patches has already been applied. Do you want to reapply them (1) or apply the $patches_context patches (2)? (1/2)"
			read -r reapply_or_continue
		done

		if [[ "$reapply_or_continue" = "1" ]]; then
			invoke_git_am "$common_patches_path"
		fi
	else
		invoke_git_am "$common_patches_path"
	fi

	if [[ -z "$common_delimiter_applied" ]]; then
		git commit --no-verify --allow-empty -m "$common_delimiter_commit_message"
	fi
	
	invoke_git_am "$patches_context_path"
}

root_dir="$(dirname $(dirname $(readlink -f $0)))"
vscode_dir="$root_dir/vscode"
patches_dir="$root_dir/vscode-patches"
build_dir="$root_dir/.build"
root_version_file="$root_dir/FULL_VERSION"
common_delimiter_commit_message="--- common patches end ---"

if [ -f $root_version_file ]; then
	full_version="$(cat $root_version_file)"
else
	# provisional value to run the build scripts outside of the CI environment
	cd $vscode_dir
	full_version="$(git rev-parse HEAD)-dev-$(TZ=UTC date '+%Y%m%d%H%M%S')"
fi

cd $root_dir

# See https://github.com/nodejs/node/issues/51555#issuecomment-1974877052
# Temporary change until we update to VSCode >=1.94.0 which replaces yarn with npm
export DISABLE_V8_COMPILE_CACHE=1

# Referenced by the build script to annotate build artifacts
export BUILD_SOURCEVERSION="$(git rev-parse HEAD:vscode)"
