#!/usr/bin/env bash

# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.

# For a tagged pipeline, use CI_COMMIT_SHA
if [ -n "${CI_COMMIT_TAG+x}" ]; then
	PATCH_PRODUCT_JSON_WITH_COMMIT_SHA="${CI_COMMIT_SHA}"
fi

# --always is required in cases where there are no tags available
# e.g. while rebasing with upstream
# https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/merge_requests/71#note_1741978758
# For a non-tagged pipeline, use the commit of the last tag if not over-ridden
if [ -z "${PATCH_PRODUCT_JSON_WITH_COMMIT_SHA+x}" ]; then
	PATCH_PRODUCT_JSON_WITH_COMMIT_SHA=$(cd "$root_dir" && git rev-list -n 1 $(git describe --abbrev=0 --tags --always))
fi

WEBVIEW_URL_KEY="webviewContentExternalBaseUrlTemplate"
WEBVIEW_URL_VALUE="https://{{uuid}}.cdn.web-ide.gitlab-static.net/web-ide-vscode/stable/${PATCH_PRODUCT_JSON_WITH_COMMIT_SHA}/out/vs/workbench/contrib/webview/browser/pre/"
PRODUCT_JSON_FILE="${vscode_dir}/product.json"

echo "Patching '${WEBVIEW_URL_KEY}' in '${PRODUCT_JSON_FILE}' to '${WEBVIEW_URL_VALUE}'"

# "sed" behaves differently on some distributions. Following approach is portable.
# https://stackoverflow.com/a/44864004
sed -i.bak "s#\"${WEBVIEW_URL_KEY}\": \".*\"#\"${WEBVIEW_URL_KEY}\": \"${WEBVIEW_URL_VALUE}\"#" "${PRODUCT_JSON_FILE}" && rm "${PRODUCT_JSON_FILE}.bak"
