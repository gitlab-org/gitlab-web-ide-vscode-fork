#!/usr/bin/env bash

# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.

# Import common variables
SCRIPTS_DIR=$(dirname $(readlink -f $0))
source "$SCRIPTS_DIR/base.sh"

cd $vscode_dir

echo "----------------"
echo "Build GitLab VSCode Fork for the web"
echo "----------------"

yarn gulp core-ci
yarn gulp vscode-web-min-ci

cd $root_dir

output_dir="${build_dir}/vscode-web"
dist_dir="${build_dir}/vscode-web-dist"

rm -rf $output_dir
mkdir -p $output_dir

# Step 1. Copy the compiled VSCode to the build directory
cp -r "${vscode_dir}/out-vscode-web-min" "${output_dir}/out"

# Step 2. Copy the compiled built-in extensions to the build directory
cp -r "${vscode_dir}/.build/web/extensions" "${output_dir}/extensions"

# Step 3. Copy the runtime web node_modules to the build directory
cp -r "${vscode_dir}/remote/web/node_modules" $output_dir
cp "${vscode_dir}/remote/web/package.json" $output_dir

# Step 4. Create distribution artifact
rm -rf $dist_dir
mkdir -p $dist_dir
bundle_and_build "${output_dir}" "${dist_dir}/vscode-web-${full_version}.tar.gz"
