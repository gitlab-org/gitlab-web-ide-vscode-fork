#!/usr/bin/env bash
set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs)
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.

# Import common variables
SCRIPTS_DIR=$(dirname $(readlink -f $0))
source "$SCRIPTS_DIR/base.sh"

patches_context="${1:-}"
patches_context=$(get_patch_context "${patches_context}")

patches_dir_tmp="${patches_dir}.tmp"

cd $root_dir

echo "[gitlab-web-ide-vscode-fork] Backing up current patches folder..."
rm -rf $patches_dir_tmp
mkdir -p $patches_dir_tmp/common
mkdir -p $patches_dir_tmp/$patches_context

echo "[gitlab-web-ide-vscode-fork] Creating patches from local submodule commits..."
cd $vscode_dir

common_delimiter_sha=$(get_common_delimiter_sha)
vscode_target_sha=$(get_vscode_target_sha)

if [ -z "$common_delimiter_sha" ]; then
  echo "[gitlab-web-ide-vscode-fork] Error! Could not find commit message in 'vscode' with '$common_delimiter_commit_message'!"
  echo "                             Did you run 'dev-apply-patches.sh'?"
  echo ""
  exit 1
fi

if [ -z "$vscode_target_sha" ]; then
  echo "[gitlab-web-ide-vscode-fork] Error! Could not find the commit sha for the base vscode version!"
  echo ""
  exit 1
fi

git format-patch --output-directory=$patches_dir_tmp/common --no-signature --abbrev=12 $vscode_target_sha...$common_delimiter_sha^1
git format-patch --output-directory=$patches_dir_tmp/$patches_context --no-signature --abbrev=12 $common_delimiter_sha...HEAD

# Did we run this without any local changes? If so, then blow up.
if [ -z "$(ls -A "$patches_dir_tmp/common")" ]; then
  echo "[gitlab-web-ide-vscode-fork] Error! Could not find local commits in \`vscode\` submodule... Did you run \`dev-apply-patches.sh\` first?"
  exit 1
fi

# **note:** We need to normalize top-line metadata. Git uses this initial header to help other UNIX'y programs reason
# with the output, so let's not just blow it away. For more info see:
# 
# - https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/45
# - https://git-scm.com/docs/git-format-patch
# 
# **side note:** See Git docs above. The date is expected to be arbitrary.
normalized_metadata="From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001"

for file in "${patches_dir_tmp}"/**/*; do
  # **note:** For cross compatability, `sed` has to write to a new file since macos handles `-i` weird
  sed "1s/.*/$normalized_metadata/" "$file" > "$file"_normalized && mv -f "$file"_normalized "$file";
done

echo "[gitlab-web-ide-vscode-fork] Copying new patches over..."
rm -rf $patches_dir/common $patches_dir/$patches_context
mv $patches_dir_tmp/common $patches_dir
mv $patches_dir_tmp/$patches_context $patches_dir
